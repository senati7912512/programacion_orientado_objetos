# Ejemplos de interfaz

## Interfaz informal 1

```python
class IAnimal:
    def insertar_datos(self, nombre:str, peso:float) -> bool:
        pass
        
    def camina(self, metros:float):
        pass
        
class Perro(IAnimal):
    def insertar_datos(self, nombre:str, peso:float):
        self.nombre = nombre
        self.peso = peso
        
        if self.peso >= 7:
            return True
        else:
            return False
    
    def camina(self, metros:float):
        if metros > 20 and metros <= 550:
            print("El perro camina lo suficiente")
        else:
            print("Tu perro está estresado, sácalo a caminar")
    
    def __repr__(self):
        return f'Tu perro {self.nombre} pesa {self.peso} kg'
        
perro = Perro()
perro2 = Perro()

perro.insertar_datos('Firulais', 7.0)
perro2.insertar_datos('Lobo', 3)
perro.camina(300)

print(perro.insertar_datos('Firulais', 7.0))
print(perro2.insertar_datos('Lobo', 3))
print(perro)
print(perro2)
```
Salida:

```sh
El perro camina lo suficiente
True
False
Tu perro Firulais pesa 7.0 kg
Tu perro Lobo pesa 3 kg
```
## Interfaz informal 2

```python
class IAnimal2:
    def insertar_datos(self, nombre:str, peso:float) -> bool:
        raise NotImplementedError
        
    def camina(self, metros:float):
        raise NotImplementedError
        
class Perro(IAnimal2):
    pass
    
perro = Perro()
perro.camina(300)

print(perro.insertar_datos("Firulais",7.0))
```

Salida:

```sh
---------------------------------------------------------------------------
NotImplementedError                       Traceback (most recent call last)
Cell In[12], line 12
      9     pass
     11 perro = Perro()
---> 12 perro.camina(300)
     14 print(perro.insertar_datos("Firulais",7.0))

Cell In[12], line 6, in IAnimal2.camina(self, metros)
      5 def camina(self, metros:float):
----> 6     raise NotImplementedError

NotImplementedError:
```

## Interfaz ABC

```python
class IAnimal:
    def insertar_datos(self, nombre:str, peso:float) -> bool:
        pass
        
    def camina(self, metros:float):
        pass
        
class Perro(IAnimal):
    def insertar_datos(self, nombre:str, peso:float):
        self.nombre = nombre
        self.peso = peso
        
        if self.peso >= 7:
            return True
        else:
            return False
    
    def camina(self, metros:float):
        if metros > 20 and metros <= 550:
            print("El perro camina lo suficiente")
        else:
            print("Tu perro está estresado, sácalo a caminar")
    
    def __repr__(self):
        return f'Tu perro {self.nombre} pesa {self.peso} kg'
        
perro = Perro()
perro2 = Perro()

perro.insertar_datos('Firulais', 7.0)
perro2.insertar_datos('Lobo', 3)
perro.camina(300)

print(perro.insertar_datos('Firulais', 7.0))
print(perro2.insertar_datos('Lobo', 3))
print(perro)
print(perro2)
```

Salida:
```sh
El perro camina lo suficiente
True
False
Tu perro Firulais pesa 7.0 kg
Tu perro Lobo pesa 3 kg
```
> NOTA: Las interfazez permiten que se implementen todos los metodos de una clase, por esto al tratar de emplear la clase sin los metodos correspondienten andará error y tambien al tratar de instanciar la clase.

Probemos: Creamos la Clase IAnimal y pero solo con un método

```python
class IAnimal2:
    def insertar_datos(self, nombre:str, peso:float) -> bool:
        raise NotImplementedError
        
    def camina(self, metros:float):
        raise NotImplementedError
        
class Perro(IAnimal2):
    pass
    
perro = Perro()
perro.camina(300)

print(perro.insertar_datos("Firulais",7.0))
```

Salida:
```sh
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
Cell In[17], line 14
     11     def __repr__(self):
     12         return f'Tu perro {self.nombre} pesa {self.peso} kg'
---> 14 perro = Perro()
     15 perro.camina(300)
     17 print(perro.insertar_datos("Firulais",7.0))

TypeError: Can't instantiate abstract class Perro with abstract method camina
```

Al tratar de instanciar el objeto:

```python
perro2 = Perro()
```

Tenemos la salida:

```sh
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
Cell In[18], line 1
----> 1 perro2 = Perro()

TypeError: Can't instantiate abstract class Perro with abstract method camina
```

# Creando una interfaz empleando *Protocol*, se puede usar a partir de python 3.8

```python
from typing import Protocol

class IAnimal(Protocol):

    # Es necesario colocar los tipe hints nombre:str, nombre:str
    def insertar_datos(self, nombre:str, peso:float) -> bool:
        pass
    def camina(self, metros:float) -> None:
        pass
class Perro(IAnimal):
    def insertar_datos(self, nombre:str, peso:float) -> bool:
        self.nombre = nombre
        self.peso = peso
        
        if self.peso >= 7:
            return True
        else:
            return False
    
    def camina(self, metros:float) -> None:
        if metros > 20 and metros <= 550:
            print("El perro camina lo suficiente")
        else:
            print("TTu perro está estresado, sácalo a caminar")
    
    def __repr__(self):
        return f'Tu perro {self.nombre} pesa {self.peso} kg'
        
perro = Perro()
perro.camina(300)

print(perro.insertar_datos("Firulais",7.0))
print(perro)
```

Salida:
```sh
El perro camina lo suficiente
True
Tu perro Firulais pesa 7.0 kg
```

