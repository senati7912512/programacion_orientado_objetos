Ejercicios propuestos herencia

1) Construir una clase Factura que descienda de la clase Precio y que incluya dos atributos
específicos llamados emisor y cliente y, al menos, un método llamado
imprimirFactura.
2) Construir una clase final Math2 que amplíe las declaraciones de métodos estáticos de la clase
Math y que incluya funciones que devuelvan, respectivamente, el máximo, el mínimo, el
sumatorio, la media aritmética y la media geométrica de un array de números reales dado como
parámetro.
3) Escribir un programa que genere un array que pueda almacenar objetos de las clases Integer,
Float, Double y Byte. Pista: Number[]x = new Number[];
