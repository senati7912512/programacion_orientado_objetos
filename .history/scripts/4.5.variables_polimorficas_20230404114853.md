# Variables Polimórficas (Polimorfismo de asignación)
Una variable polimórfica es aquélla que puede referenciar más de un tipo de objeto
Puede mantener valores de distintos tipos en distintos momentos de ejecución del programa.
En un lenguaje con sistema de tipos dinámico todas las variables son potencialmente polimórficas
En un lenguaje con sistema de tipos estático la variable polimórfica es la materialización del principio de sustitución.
En Java: las referencias a objetos.
En C++: punteros o referencias a clases polimórficas
