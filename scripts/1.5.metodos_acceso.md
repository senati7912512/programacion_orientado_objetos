# Modificadores el acceso en Python

Python no tiene soporte para modificadores de acceso en tiempo de ejecución, pero tiene un soporte bastante bueno para modificadores de acceso en "tiempo de compilación" a través de linters y verificadores de tipos.

En esta breve publicación cubriremos las principales formas en que uno puede imitar los modificadores de acceso en Python. Cubriremos atributos y métodos privados y protegidos, atributos de solo lectura y métodos y variables finales.

Para este post, el código se comprueba con el gran servidor de lenguaje basado en Pylance en vscode. Por lo tanto, otros IDE o verificadores / linters estáticos pueden no dar las mismas advertencias / errores. También hago uso de la frase "tiempo de compilación", que solo significa que en el momento en que se ejecutan linters o verificadores de tipos.

## Privado vs Protegido

La convención de "subrayado" (es decir, nombrar una variable o método en lugar de ) representar un valor protegido y un valor privado. Python tiene soporte en de tiempo de ejecución para variables privadas a través de un doble guión bajo (es decir, ). ¡En realidad generará un error! _name name__name

Como recordatorio, privado significa que el atributo/método solo se puede usar dentro de la clase donde se define. Protegido significa que el atributo/método sólo se puede utilizar en la clase donde está definido o sus subclases.

Ejemplo:

```python
class Thing:
    def __init__(self, public: str, *, protected: str = "protected", private: str = "private"):
        self.public = public
        self._protected = protected
        self.__private = private

    def info(self) -> None:
        print(
            (
                f"This class has public attribute: {self.public}, "
                f"protected attribute: {self._protected}, "
                f"and private attribute: {self.__private}"
            )
        )
```
Salida:

```sh
>>> thing = Thing("public")

# this is fine because it is assessing the variables internally in the info method
>>> thing.info()
'This class has public attribute: public, protected attribute: protected, and private attribute: private'

# this is also fine because the public attribute is indeed public
>>> print(thing.public)
'public'

# this will run but will give an error when checked with pylance
>>> print(thing._protected)
'"_protected" is protected and used outside of the class in which it is declared'

# this will not actually run and will raise an AttributeError but it will also give an error when checked
>>> print(thing.__private)
'"__private" is private and used outside of the class in which it is declared'
```

Aquí tenemos una clase simple con tres atributos. Uno es público, otro está protegido y el otro es privado. Podemos ver que denotamos "protegido" con un solo guión bajo antes del nombre de la variable y "privado" con un doble guión bajo antes del nombre de la variable.Thing

Lo primero que hay que tener en cuenta es que si define un atributo privado, debe usarlo en la clase, ya que ese es el único lugar donde se puede usar. Si no lo hace, probablemente recibirá una advertencia como "no se accede a __private".

```python
class SomeThing(Thing):
    def more_info(self) -> None:
        print(f"This class has public attribute: {self.public}, protected attribute: {self._protected}")
```
Salida:

```sh
>>> some_thing = SomeThing("public")

# still can use the info method which uses the private attribute internally
>>> some_thing.info()
'This class has public attribute: public, protected attribute: protected, and private attribute: private'

# can use the new more_info method that uses the public and protected attribute
>>> some_thing.more_info()
'This class has public attribute: public, protected attribute: protected'
```
Hemos hecho una subclase de y tenemos un método simple que de hecho puede usar el atributo. Sin embargo, si intentamos agregar un nuevo método que use el atributo private, sucederán cosas malas. Thing_protected

```python
class SomeThing(Thing):
    def more_info(self) -> None:
        print(f"This class has public attribute: {self.public}, protected attribute: {self._protected}")

    def use_private(self) -> None:
        print(f"Private attribute is {self.__private}")
```
Salida:

```sh
>>> some_thing = SomeThing("public")

# this will raise an AttributeError and will also give an error when checked
>>> some_thing.use_private()
'"__private" is private and used outside of the class in which it is declared'
```
no podemos usar el atributo en la subclase, como se esperaba. __private

Se muestra un ejemplo básico de cómo usar atributos protegidos y privados. Las mismas reglas funcionan para los métodos protegidos y privados. Pero, ¿cuándo deberías usarlos? No hay reglas estrictas y rápidas para esto, pero hay algunas reglas generales y cosas que debe tener en cuenta.

Las variables protegidas y privadas son parte de un concepto conocido como ocultación de información que se ocupa de ocultar los detalles de implementación de los usuarios intermedios.

Los atributos/métodos privados deben usarse en los casos en los que no desea que los usuarios intermedios o los desarrolladores tengan acceso a ese atributo o método. Esto es bueno para ocultar detalles de implementación que pueden ser propensos a cambiar pero que no afectarán a los usuarios intermedios.

Los atributos/métodos protegidos deben usarse donde los desarrolladores pueden tener acceso (a través de subclases) pero no los usuarios externos. Esto es útil para definir métodos que implementarán las subclases (a través de ABC) que luego se usan en la clase principal a través de un mecanismo de reutilización de código. Consulte mi publicación anterior para obtener más información sobre este caso de uso.

Si usa atributos/métodos protegidos y permite la creación de subclases, estos atributos/métodos se vuelven esencialmente parte de la API pública, ya que otros desarrolladores pueden tener acceso a ellos en sus subclases. Esto significa que un cambio en la implementación protegida en la clase principal afectará a todas las subclases. Más adelante veremos cómo podemos protegernos contra subclases no deseadas con el decorador. @final

## Atributos de solo lectura

En el ejemplo anterior, el atributo público era lectura/escritura. ¿Qué pasa si queremos un atributo de solo lectura? Hay dos formas de hacer esto, una se aplica en tiempo de ejecución y la otra se aplica a través de la verificación de tipo en "tiempo de compilación". Modifiquemos nuestro ejemplo anterior para incluir un atributo de solo lectura.

```python
class Thing:
    def __init__(self, readonly: str):
        self.__readonly = readonly

    @property
    def readonly(self) -> str:
        return self.__readonly
```
Salida:

```sh
>>> thing = Thing("readonly")
>>> print(thing.readonly)
'readonly'

# this will raise an AttributeError but will also raise an error when checking
>>> thing.readonly = "Hello!"
'Cannot assign member "readonly" for type "Thing" Property "readonly" has no defined setter'
```
Hemos utilizado un atributo que nos permite crear de solo lectura. Esto generará un error de tiempo de ejecución y un error de "tiempo de compilación" al intentar establecer este atributo. Sin embargo, requiere el uso de un atributo privado (también podría usar protected, asegurándose de prestar atención a las advertencias mencionadas anteriormente) y el uso de una propiedad. Hay una forma más corta usando type @property readonl Final.

```python
from typing import Final


class Thing:
    def __init__(self, readonly: str = "readonly"):
        self.readonly: Final = readonly
```
Salida:

```sh
>>> thing = Thing("readonly")
>>> print(thing.readonly)
'readonly'

# this will not raise a runtime error but will raise an error when checking
>>> thing.readonly = "Hello!"
'Cannot assign member "readonly" for type "Thing" "readonly" is declared as Final and cannot be reassigned'
```
## Clases y métodos finales

En la última sección, vimos cómo usar las anotaciones de tipo para hacer que las variables sean de solo lectura, al menos en un sentido de verificación de tipo estático. En esta última sección veremos cómo hacer que las clases y los métodos sean más restringidos en términos de lo que pueden ser subclases o anularse. Volvamos a nuestra clase Final Thing.

```python
from typing import final


@final
class Thing:
    def __init__(self, public: str, *, protected: str = "protected", private: str = "private"):
        self.public = public
        self._protected = protected
        self.__private = private

    def info(self) -> None:
        print(
            (
                f"This class has public attribute: {self.public}, "
                f"protected attribute: {self._protected}, "
                f"private attribute: {self.__private}, "
            )
        )
```
Esto es idéntico a nuestra definición anterior, pero hemos agregado el decorador. Si no tratamos de subclasificar esta clase @final

```python
class SomeThing(Thing):
    pass
```
Luego obtendremos un error (a través de la verificación de tipo estático, no en tiempo de ejecución) "La clase base" Cosa "se marca como final y no se puede subclasificar". También debemos tener en cuenta que si marcamos una clase como final, entonces no hay necesidad de distinguir entre variables protegidas y privadas, ya que técnicamente ambas tienen el mismo significado ahora, ya que la clase no se puede dividir en subclases.

También puede usar el decorador para controlar qué métodos se pueden anular: @final

```python
class Thing:
    def __init__(self, public: str, *, protected: str = "protected", private: str = "private"):
        self.public = public
        self._protected = protected
        self.__private = private

    @final
    def info(self) -> None:
        print(
            (
                f"This class has public attribute: {self.public}, "
                f"protected attribute: {self._protected}, "
                f"private attribute: {self.__private}, "
            )
        )
```
Cuando subcontratamos e intentamos anular el método de información.

```python
class SomeThing(Thing):
    def info(self) -> None:
        print("Overriding info method")
```
Obtendríamos el siguiente error: "El método "info" no puede anular el método final definido en la clase "Cosa"".

Ambos métodos pueden ser útiles cuando realmente desea controlar lo que los users/developers intermedios pueden hacer con sus clases y métodos.

## Conclusión

En esta publicación, hemos visto cómo hacer que los atributos/métodos sean privados y protegidos y las advertencias asociadas con eso. También hemos visto cómo usar un decorador y una anotación para hacer que un atributo o variable sea de solo lectura. Finalmente (¡¿entiendes?!) hemos visto cómo usar el decorador para marcar una clase o un método como final, impidiendo así que sea subclasificado o anulado.@property Final @final

Por último, voy a ofrecer algunos consejos. En el mundo del software de código abierto, debemos pensar de manera diferente sobre lo que es realmente privado, público y protegido. No queremos restringir demasiado el acceso para que la vida de los desarrolladores sea más difícil cuando intentan extender su código, pero tampoco queremos crear una gran superficie de API con muchos métodos y atributos públicos innecesarios que deben ser mantenido compatible con versiones anteriores. Entonces, ahora que sabe cómo usar estas funciones en Python, piense detenidamente cómo las usará y recuerde: "Un gran poder conlleva una gran responsabilidad".